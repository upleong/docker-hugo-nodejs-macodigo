FROM debian:jessie-slim
MAINTAINER UP Leong <pio.leong@gmail.com>

# Update apt
RUN apt-get update


# Install Hugo and utilities
RUN apt-get install -y curl git xz-utils


ENV GOLANG_VERSION 1.8
ENV GOLANG_DOWNLOAD_URL https://golang.org/dl/go$GOLANG_VERSION.linux-amd64.tar.gz
ENV GOLANG_DOWNLOAD_SHA256 53ab94104ee3923e228a2cb2116e5e462ad3ebaeea06ff04463479d7f12d27ca

RUN curl -fsSL "$GOLANG_DOWNLOAD_URL" -o golang.tar.gz \
	&& echo "$GOLANG_DOWNLOAD_SHA256  golang.tar.gz" | sha256sum -c - \
	&& tar -C /usr/local -xzf golang.tar.gz \
	&& rm golang.tar.gz

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH

COPY go-wrapper /usr/local/bin/
RUN go version

# Install Hugo
RUN go get -v github.com/spf13/hugo
RUN hugo version

# Install nodejs from binaries for space saving
ENV NODEJS_VERSION v6.10.2
RUN curl -fSL "https://nodejs.org/dist/$NODEJS_VERSION/node-$NODEJS_VERSION-linux-x64.tar.xz" -o nodejs.tar.xz \
    && tar -C /usr/local -xf nodejs.tar.xz \
    && rm nodejs.tar.xz
ENV PATH $PATH:/usr/local/node-$NODEJS_VERSION-linux-x64/bin
RUN node --version

# Install Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH /root/.cargo/bin:$PATH
RUN rustc --version

# Cleanup Apt
RUN apt-get clean
#RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Er, sure, why not
CMD ["/bin/bash"]
