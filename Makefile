all: build

build:
	docker build -t macodigo/hugo-nodejs-macodigo .

push:
	docker push macodigo/hugo-nodejs-macodigo
